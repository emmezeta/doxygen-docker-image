FROM alpine:latest

LABEL maintainer="Matteo Mazza <matteom.mazza@gmail.com>"

RUN apk --no-cache add doxygen
